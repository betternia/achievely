-- CreateTable
CREATE TABLE "UserAccount" (
    "sid" SERIAL NOT NULL,
    "uid" VARCHAR(40) NOT NULL,
    "source" VARCHAR(60) NOT NULL,
    "sourceAccountId" VARCHAR(125) NOT NULL,
    "registDate" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "syncDate" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "sourceProfile" TEXT,
    "lastAccessDate" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "passcode" VARCHAR(40),
    "loginFailCount" INTEGER,
    "userUid" VARCHAR(40) NOT NULL,

    PRIMARY KEY ("uid")
);

-- CreateTable
CREATE TABLE "User" (
    "sid" SERIAL NOT NULL,
    "uid" VARCHAR(40) NOT NULL,
    "id" VARCHAR(60) NOT NULL,
    "givenName" VARCHAR(120) NOT NULL,
    "familyName" VARCHAR(120) NOT NULL,
    "additionalName" VARCHAR(120),
    "alternateName" VARCHAR(120),
    "address" TEXT,
    "description" TEXT,
    "birthDate" VARCHAR(10),
    "birthPlace" VARCHAR(120),
    "image" VARCHAR(120),
    "url" VARCHAR(120),

    PRIMARY KEY ("uid")
);

-- CreateTable
CREATE TABLE "NoemaTag" (
    "sid" SERIAL NOT NULL,
    "uid" VARCHAR(40) NOT NULL,
    "type" VARCHAR(30) NOT NULL,
    "name" VARCHAR(30) NOT NULL,
    "noemaUid" VARCHAR(40),

    PRIMARY KEY ("uid")
);

-- CreateTable
CREATE TABLE "NoemaAttachment" (
    "sid" SERIAL NOT NULL,
    "uid" VARCHAR(40) NOT NULL,
    "type" VARCHAR(30) NOT NULL,
    "data" VARCHAR(2000) NOT NULL,
    "noemaUid" VARCHAR(40) NOT NULL,

    PRIMARY KEY ("uid")
);

-- CreateTable
CREATE TABLE "Noema" (
    "sid" SERIAL NOT NULL,
    "uid" VARCHAR(40) NOT NULL,
    "status" INTEGER,
    "userUid" VARCHAR(40) NOT NULL,
    "createdDate" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "modifiedDate" TIMESTAMP(3),
    "kind" TEXT NOT NULL,
    "eventDate" TIMESTAMP(3) NOT NULL,
    "title" VARCHAR(1000),
    "content" TEXT NOT NULL,
    "highlighted" BOOLEAN DEFAULT false,
    "mood" INTEGER,
    "location" VARCHAR(2000),
    "category" VARCHAR(30),
    "activity" VARCHAR(60),
    "trunkUid" VARCHAR(40),

    PRIMARY KEY ("uid")
);

-- CreateIndex
CREATE UNIQUE INDEX "UserAccount.source_sourceAccountId_unique" ON "UserAccount"("source", "sourceAccountId");

-- CreateIndex
CREATE UNIQUE INDEX "User.id_unique" ON "User"("id");

-- AddForeignKey
ALTER TABLE "UserAccount" ADD FOREIGN KEY ("userUid") REFERENCES "User"("uid") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "NoemaTag" ADD FOREIGN KEY ("noemaUid") REFERENCES "Noema"("uid") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "NoemaAttachment" ADD FOREIGN KEY ("noemaUid") REFERENCES "Noema"("uid") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Noema" ADD FOREIGN KEY ("userUid") REFERENCES "User"("uid") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Noema" ADD FOREIGN KEY ("trunkUid") REFERENCES "Noema"("uid") ON DELETE SET NULL ON UPDATE CASCADE;
