import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  Put,
  Delete,
} from '@nestjs/common';
import { UserService } from './user.service';

import { Prisma, User, UserAccount } from '@prisma/client';


@Controller()
export class UserController {
  constructor(
    private readonly userService: UserService
  ) { }

  @Get('users/:xid')
  async getUserById(@Param('xid') xid: string): Promise<User> {
    console.debug({op: 'getUserById', param: xid});
    let user = await this.userService.user({ id: xid }) ?? await this.userService.user({ uid: xid });
    return user;
  }

  @Get('accounts/:uid')
  async getUserAccountByUid(@Param('uid') uid: string): Promise<UserAccount> {
    console.debug({op: 'getUserAccountByUid', param: uid});
    let account = await this.userService.account({ uid }) 
    console.debug('FOUND: ' + JSON.stringify(account, null, 2));
    return account;
  }

  @Post('register-account')
  async registerAccount(
    @Body() postData: { 
      account: Prisma.UserAccountCreateInput, 
      user?: Prisma.UserCreateInput 
    }
  ): Promise<UserAccount> {
    return this.userService.registerAccount(postData.account, postData.user);
  }
}
