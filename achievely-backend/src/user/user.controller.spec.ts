import * as request from 'supertest';
import { Test, TestingModule } from '@nestjs/testing';
import { PrismaService } from '../prisma.service';
import { UserController } from './user.controller';
import { UserService } from './user.service';

import { INestApplication } from '@nestjs/common';
import { UserAccount } from '@prisma/client';
import { randStr, newAccount, newUser } from './user.test.utils';

describe('UserController', () => {
  let app: INestApplication;
  let userController: UserController;
  let userService: UserService;

  beforeAll(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      controllers: [UserController],
      providers: [PrismaService, UserService],
    }).compile();

    userService = moduleRef.get<UserService>(UserService);
    userController = moduleRef.get<UserController>(UserController);

    app = moduleRef.createNestApplication();
    await app.init();

  });

  afterAll(async () => {
    await app.close();
  });

  describe('endpoints', () => {
    it('[GET] /users/:xid', async () => {

      const account = await createAccount('CtlrTest', 'T1');
      // console.log('IN:' + JSON.stringify(account, null, 2));

      await request(app.getHttpServer())
        .get(`/users/${account.userUid}`)
        .expect(200)
        .expect(function (res) {
           expect(res.body.uid).toBe(account.userUid);
        });
      
      await userService.deleteUserByUid(account.userUid);
    });

    it('[GET] /accounts/:xid', async () => {

      const account = await createAccount('CtlrTest', 'T2');

      await request(app.getHttpServer())
        .get(`/accounts/${account.uid}`)
        .expect(200)
        .expect(function (res) {
          let act = {...res.body} 
          act.lastAccessDate = new Date(act.lastAccessDate);
          act.registDate = new Date(act.registDate);
          act.syncDate = new Date(act.syncDate);
          expect(act).toEqual(account);
        });
      await userService.deleteUserByUid(account.userUid);
    });
  });

  async function createAccount(source: string, idPrefix: string): Promise<UserAccount> {
    const rands = randStr();
    const createdAccount = await userService.registerAccount(
      newAccount(source, `${idPrefix}-${rands}`),
      newUser(`${idPrefix}-${rands}`));
    return createdAccount;
  }
});
