import { Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma.service';
import {
  User,
  UserAccount,
  Prisma
} from '@prisma/client';

@Injectable()
export class UserService {
  constructor(private prisma: PrismaService) { }

  async account(userAccountWhereUniqueInput: Prisma.UserAccountWhereUniqueInput): Promise<UserAccount | null> {
    return this.prisma.userAccount.findUnique({
      where: userAccountWhereUniqueInput,
    });
  }

  async user(userWhereUniqueInput: Prisma.UserWhereUniqueInput): Promise<User | null> {
    return this.prisma.user.findUnique({
      where: userWhereUniqueInput,
    });
  }

  async users(params: {
    skip?: number;
    take?: number;
    cursor?: Prisma.UserWhereUniqueInput;
    where?: Prisma.UserWhereInput;
    orderBy?: Prisma.UserOrderByInput;
  }): Promise<User[]> {
    const { skip, take, cursor, where, orderBy } = params;
    return this.prisma.user.findMany({
      skip,
      take,
      cursor,
      where,
      orderBy,
    });
  }

  async createUser(data: Prisma.UserCreateInput): Promise<User> {
    return this.prisma.user.create({
      data,
    });
  }


  /**
   * Registers a new account if it is new.
   * If account already exists, returned the entity.
   * Otherwise, if account does not exist:
   * 1. Retrieve or create a user based on the information provided
   * 2. Create account associating with the user
   * 
   * @param userAccountInput 
   * @param userInput 
   */
  async registerAccount(
    userAccountInput: Prisma.UserAccountCreateInput,
    userInput?: Prisma.UserCreateInput
  ): Promise<UserAccount> {
    const account = await this.prisma.userAccount.findUnique({
      where: {
        source_sourceAccountId: {
          source: userAccountInput.source,
          sourceAccountId: userAccountInput.sourceAccountId,
        }
      },
      include: {
        user: true
      }
    });

    if (account) {
      return account;
    }

    // New account, register with the user or associate with existing user

    if (!userInput?.id) {
      throw Error('User id cannot be null');
    }

    let user: User = await this.prisma.user.findUnique({
      where: { id: userInput?.id }
    }) || await this.prisma.user.create({
      data: userInput,
    });

    if (!user) {
      throw Error('User not found');
    }

    userAccountInput.user = {
      connect: { uid: user.uid }
    };

    return await this.prisma.userAccount.create({
      data: userAccountInput,
    });;
  }

  async updateUser(params: {
    where: Prisma.UserWhereUniqueInput;
    data: Prisma.UserUpdateInput;
  }): Promise<User> {
    const { data } = params;
    const where: Prisma.UserWhereUniqueInput = params.where ? params.where : { uid: data.uid as string }
    return this.prisma.user.update({
      data,
      where,
    });
  }

  async deleteUserByUid(userUid: string): Promise<User> {

    await this.prisma.userAccount.deleteMany({where: { userUid: userUid}})

    // {uid: createdAccount.userUid}
    return this.prisma.user.delete({
      where: {uid: userUid}
    });
  }
}
