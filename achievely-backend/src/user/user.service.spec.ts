import { Test, TestingModule } from '@nestjs/testing';
import { PrismaService } from '../prisma.service';
import { UserService } from './user.service';

import {
  newAccount, newUser
} from './user.test.utils';

describe('UserService', () => {
  let app: TestingModule;
  let userService: UserService;

  beforeAll(async () => {
    app = await Test.createTestingModule({
      providers: [UserService, PrismaService],
    }).compile();

    userService = app.get<UserService>(UserService);
  });

  afterAll(async () => {
    await app.close();
  });


  describe('root', () => {
    it('WHEN createUser, THEN user is in db', async () => {
      const createdUser = await userService.createUser(newUser('T1'));
      expect(createdUser).not.toBeNull();

      // Remove
      await userService.deleteUserByUid(createdUser.uid);
    });

    it('WHEN registerAccount, THEN user and account is in db', async () => {
      const createdAccount = await userService.registerAccount(newAccount('test', 'A1'), newUser('T12'));
      expect(createdAccount).not.toBeNull();

      // Find user
      const foundUser = await userService.user({uid: createdAccount.userUid});
      expect(foundUser).not.toBeNull();

      // Remove
      await userService.deleteUserByUid(createdAccount.userUid);  // Account will delete cascade
    });

  });

});
