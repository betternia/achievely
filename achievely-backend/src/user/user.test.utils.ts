import {
  Prisma
} from '@prisma/client';

// Simple random string generator (9 chars)
export function randStr(): string {
  return Math.random().toString(36).substr(2, 9);
} 

// Auxiliary functions:
export function newAccount(source: string, accountId: string): Prisma.UserAccountCreateInput {
  return {
    source: source,
    sourceAccountId: accountId,
    sourceProfile: '',
    user: undefined
  }
}

export function newUser(name: string, familyName: string='Tester'): Prisma.UserCreateInput {
  return {
    id: name,
    givenName: name,
    familyName: familyName
  }
}
