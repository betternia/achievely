import { Pageable } from '../foundation/pagination';
import { Prisma} from '@prisma/client';
import { pageableToPrismaParams } from './prisma.adapter'

describe('prisma.adapter', () => {
  describe('pageableToPrismaParams', () => {
    it('WHEN createNoema, THEN noema and tags is in db', async () => {

      const pageable = new Pageable(3, 5, ['uid:asc', 'kind:desc']);

      const where: Prisma.NoemaWhereInput = {
        uid: 'abc',
        status: 3,
        kind: 'KIND'
      };

      const findParams = pageableToPrismaParams(pageable, where);
      // console.log(JSON.stringify(findParams, null, 2));

      const expected = {
        skip: 15,
        take: 5,
        where: where,
        orderBy: {
          uid: 'asc',
          kind: 'desc'
        }
      }

      const prismaOrderBy = findParams.orderBy as Prisma.NoemaOrderByInput

      expect(findParams).toEqual(expected);
      expect(prismaOrderBy).toEqual(expected.orderBy);
    })
  });
});
