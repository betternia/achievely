import { Test, TestingModule } from '@nestjs/testing';
import { PrismaService } from '../prisma.service';
import { UserService } from '../user/user.service';
import {
  Prisma,
  User,
  UserAccount,
  Noema
} from '@prisma/client';
import { randStr, newAccount, newUser } from '../user/user.test.utils';

// Service under test
import { NoemaService } from './noema.service';
import { Pageable } from '../foundation/pagination';

describe('NoemaService', () => {
  let app: TestingModule;
  let userService: UserService;
  let noemaService: NoemaService;

  beforeAll(async () => {
    const app: TestingModule = await Test.createTestingModule({
      providers: [NoemaService, UserService, PrismaService],
    }).compile();

    noemaService = app.get<NoemaService>(NoemaService);
    userService = app.get<UserService>(UserService);
  });

  afterAll(async () => {
    await app.close();
  });

  describe('CRUD', () => {
    it('WHEN createNoema, THEN noema and tags is in db', async () => {

      const account = await createAccount('test-noema', 't1');

      // Create
      const createdNoema = await noemaService.createNoema(newNoema('This is a test'), account, { tags: ['a', 'b'] });
      expect(createdNoema).not.toBeNull();

      // Query
      const foundNoemas = await noemaService.noemas({ where: { userUid: account.userUid } });
      expect(foundNoemas.length).toEqual(1);
      expect(foundNoemas[0].userUid).toEqual(account.userUid);
      expect(foundNoemas[0]['tags'].length).toEqual(2);

      // Update
      const data: Prisma.NoemaUpdateInput = {
        content: 'Modified content'
      };
      const updatedNoema = await noemaService.updateNoema({
        where: { uid: foundNoemas[0].uid },
        data,
        tags: ['b', 'c', 'd']
      });

      // Query updated
      const foundNoema = await noemaService.noema({ uid: foundNoemas[0].uid });
      expect(foundNoema.content).toEqual('Modified content');
      expect(foundNoema.userUid).toEqual(account.userUid);
      expect(foundNoema['tags'].length).toEqual(3);

      await noemaService.deleteNoema({uid: createdNoema.uid});

      await deleteUser(account);
    });

    it('WHEN query, THEN return page', async () => {
      const account = await createAccount('test-noema', 't2');

      // Create
      let noemas: Noema[] = [];
      for (let i = 0; i < 10; i++) {
        noemas.push(
          await noemaService.createNoema(newNoema(`N-${i}`), account, { tags: ['tag'] })
        );
      }
      const pageable = new Pageable(0, 4, ['eventDate:desc']);
      const noemasPage = await noemaService.queryNoema({ where: { userUid: account.userUid }, pageable });
      const foundNoemaUids = noemasPage.content.map(x => x.uid);
      
      const reversed = noemas.slice(6).reverse();
      const expectedUids = reversed.map(x => x.uid);

      // console.debug('**:' + JSON.stringify(noemasPage, null, 2));
      // console.debug('**:' + JSON.stringify(noemas, null, 2));

      expect(noemasPage.size).toEqual(4);
      expect(noemasPage.totalElements).toEqual(10);
      expect(noemasPage.number).toEqual(0);
      expect(noemasPage.totalPages).toEqual(3);

      expect(noemasPage.content.length).toEqual(4);
      expect(foundNoemaUids).toEqual(expectedUids);

      for (let n of noemas) {
        await noemaService.deleteNoema({uid: n.uid});
      }
      await deleteUser(account);
    });
  });

  describe('helper functions', () => {
    describe('tagsDiff', () => {
      it('WHEN originalTags and newTags have differences, THEN return added and removed', async () => {

        const a: Prisma.NoemaTagCreateInput[] = [
          newNoemaTag('a', 'UID1'),
          newNoemaTag('b'),
          newNoemaTag('c'),
        ];
        const b: Prisma.NoemaTagCreateInput[] = [
          newNoemaTag('b'),
          newNoemaTag('c'),
          newNoemaTag('d'),
        ];

        const diff = noemaService.tagsDiff(a, b);
        expect(diff.removed.map(x => x.name).sort()).toEqual(['a']);
        expect(diff.added.map(x => x.name).sort()).toEqual(['d']);
      });

      it('WHEN originalTags and newTags are same, THEN return empty added and removed', async () => {

        const a: Prisma.NoemaTagCreateInput[] = [
          newNoemaTag('a', 'UID1'),
          newNoemaTag('b'),
        ];
        const b: Prisma.NoemaTagCreateInput[] = [
          newNoemaTag('a'),
          newNoemaTag('b'),
        ];

        const diff = noemaService.tagsDiff(a, b);
        expect(diff.removed.map(x => x.name).sort()).toEqual([]);
        expect(diff.added.map(x => x.name).sort()).toEqual([]);
      });

      it('WHEN originalTags is superset of newTags, THEN return empty added', async () => {

        const a: Prisma.NoemaTagCreateInput[] = [
          newNoemaTag('a', 'UID1'),
          newNoemaTag('b'),
        ];
        const b: Prisma.NoemaTagCreateInput[] = [
          newNoemaTag('b'),
        ];

        const diff = noemaService.tagsDiff(a, b);
        expect(diff.removed.map(x => x.name).sort()).toEqual(['a']);
        expect(diff.added.map(x => x.name).sort()).toEqual([]);
      });

      it('WHEN originalTags is empty of newTags, THEN return added only', async () => {

        const a: Prisma.NoemaTagCreateInput[] = [];
        const b: Prisma.NoemaTagCreateInput[] = [
          newNoemaTag('a'),
          newNoemaTag('b'),
        ];

        const diff = noemaService.tagsDiff(a, b);
        expect(diff.removed.map(x => x.name).sort()).toEqual([]);
        expect(diff.added.map(x => x.name).sort()).toEqual(['a', 'b']);
      });
    });
  });

  // Auxiliary functions:
  function newNoema(content: string): Prisma.NoemaCreateInput {
    return {
      kind: 'noema',
      eventDate: new Date(),
      content: content,
      user: undefined
    }
  }

  async function createAccount(source: string, idPrefix: string): Promise<UserAccount> {
    const rands = randStr();
    const createdAccount = await userService.registerAccount(newAccount(source, `${idPrefix}-${rands}`), newUser(`${idPrefix}-${rands}`));
    return createdAccount;
  }

  async function deleteUser(account: UserAccount): Promise<User> {
    const deletedUser = await userService.deleteUserByUid(account.userUid);
    return deletedUser;
  }

  function newNoemaTag(name: string, uid?: string, type: string = 'test'): Prisma.NoemaTagCreateInput {
    return {
      uid,
      name,
      type,
      noema: undefined
    };
  }
});
