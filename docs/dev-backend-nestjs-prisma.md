Developing the backed: NestJS + Prisma
======================================

## Following the Recipe
Recipe link: https://docs.nestjs.com/recipes/prisma

### 1. Install nestjs cli and generate a new nest project skeleton with it.
```shell
yarn global add @nestjs/cli
# or: npm install -g @nestjs/cli
nest new achievely-backend
```

### 2. Initialize prisma
```
yarn add prisma --dev
npx prisma init
```
Modify `.env`, `prisma/schema.prisma` with the appropriate db connection information.

To run postgres with docker on command line
```shell
docker run --rm --name ach-postgres -p 5432:5432 -e POSTGRES_USER=achieve -e POSTGRES_PASSWORD=achievepwd -e POSTGRES_DB=achievely-local -d postgres
```

### 3. Generate migration and run on dev
Modify `schema.prisma` adding the data model. 
> NOTE: at this moment, only single `schema.prisma` file is supported.

```
npx prisma migrate dev --name init --preview-feature
```
The command should have generated a folder named `{timestamp}_{name}` under `prisma/migrations`. The newly created folder should include an sql file. 

### 4. Install and generate Prisma client
```sh
$ yarn add @prisma/client
```
The installation automatically runs `npx prisma generate`. You need to run this command after every change to your Prisma models to update your generated Prisma Client.

> NOTE: The prisma generate command reads your Prisma schema and updates the generated Prisma Client library inside `node_modules/@prisma/client`.

## Gotchas

- In order to use the createMany, the preview feature must be enabled in `prisma.schema`:
  ```javascript
  previewFeatures = ["createMany"]
  ```
