import { DateTime } from 'luxon';

/**
 * Serialized date
 * Nothing special, just wanted a short syntax.
 */
export function dateSer(): string {
  return (new Date()).toJSON();
}

/**
 * Return a string with simple time format
 * @param dateSr 
 */
export function formatDateTime(dateSr: string): string {
  return DateTime.fromISO(dateSr).toLocaleString(DateTime.DATETIME_SHORT);
}

/**
 * Return a string with simple time format
 * @param dateSr 
 */
export function formatTime(dateSr: string): string {
  return DateTime.fromISO(dateSr).toLocaleString(DateTime.TIME_SIMPLE);
}
