import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { AppThunk, AppDispatch } from '../../store'
import { Account, Session } from './session.types'

import { loadUserData } from './session.service';


const initialState: Session = {
  loading: false,
  account: null,
  loginTimestamp: null
};

const sessionSlice = createSlice({
  name: 'session',
  initialState,
  reducers: {
    setLoading(state/*, action: PayloadAction<boolean>*/) {
      state = { loading: true, account: null, loginTimestamp: null };
      return state;
    },
    setSession(state, action: PayloadAction<Account>) {

      state = { loading: false, account: action.payload, loginTimestamp: Date.now() };
      console.log(JSON.stringify(state, null, 2));
      return state;
    },
    removeSession(state) {
      state = initialState;
    }
  }
});

function auth0UserToAccount(user: any): Account | null {
  return user ? {
    givenName: user.given_name,
    familyName: user.family_name,
    nickname: user.nickname,
    pictureUrl: user.picture,
    locale: user.locale,
    updatedAt: user.updated_at,
    email: user.email,
    sub: user.sub,
  } : null;
}

export const setSession = (user: any): AppThunk => async (dispatch: AppDispatch) => {

  const account = auth0UserToAccount(user);
  if (account) {
    dispatch(sessionSlice.actions.setSession(account))

    // TODO: fetch user data
    await loadUserData(dispatch);
  }
}

export const { setLoading, removeSession } = sessionSlice.actions;

export default sessionSlice.reducer;
