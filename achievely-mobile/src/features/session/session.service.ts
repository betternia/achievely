import { AppDispatch } from '../../store'

import { addDayNoema, addDayTask } from '../daily/daily.slice'
import { getNoemas } from '../noema/noema.service'

export async function loadUserData(dispatch: AppDispatch) {
  const noemas = await getNoemas();

  for (const noema of noemas) {
    if (noema.kind === 'noema') {
      dispatch(addDayNoema(noema));
    } else {
      dispatch(addDayTask(noema));
    }

  }
}
