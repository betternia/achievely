export interface Account {
  givenName: string;
  familyName: string;
  nickname: string;
  pictureUrl: string;
  locale: string;
  updatedAt: string;
  email: string[];
  sub: string;
}

export interface Session {
  loading: boolean,
  account: Account | null,
  loginTimestamp: number | null, // epoch
}
