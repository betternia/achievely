import React from 'react';
import { useDispatch } from  'react-redux'
import { useForm } from 'react-hook-form';

import { IonItem, IonLabel, IonInput, IonTextarea, IonButton } from '@ionic/react';

import { addDayTask } from '../daily/daily.slice';
import { Task } from './noema.types';

interface TaskFormProps {
  entry?: Task,
  onComplete: () => void
}

export default function TaskForm({entry, onComplete}: TaskFormProps): JSX.Element {
  const dispatch = useDispatch();

  const { register, setValue, handleSubmit, errors } = useForm<Task>();
  const onSubmit = handleSubmit( (formData) => {
    // console.log(firstName, lastName);
    formData.kind = 'task';
    formData.highlighted = entry?.highlighted;
    formData.eventDate = entry ? entry.eventDate : (new Date()).toJSON();
    formData.kind = 'task';
    
    if (entry) {
      dispatch(addDayTask(formData));
    } else {
      // TODO: update
      dispatch(addDayTask(formData));
    }

    onComplete();
  }); // firstName and lastName will have correct type

  return (
    <form onSubmit={onSubmit}>
      Task Form
      <IonItem>
        <IonLabel position="stacked" color="primary">Kind</IonLabel>
        <IonInput name="kind" type="text" ref={register} 
          placeholder="kind"
          autocapitalize="off" required>
        </IonInput>
        <IonLabel position="stacked" color="primary">Title</IonLabel>
        <IonInput name="title" type="text" ref={register}
          placeholder="title"
          autocapitalize="off" required>
        </IonInput>
        <IonLabel position="stacked" color="primary">Content</IonLabel>
        <IonTextarea name="content" ref={register}
          placeholder="content"
          autocapitalize="off" rows={5} required>
        </IonTextarea>
        <IonButton type="submit">Submit</IonButton>
      </IonItem>
    </form>
  )
} 
