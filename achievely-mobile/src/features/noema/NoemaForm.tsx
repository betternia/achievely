import React from 'react';
import { useDispatch } from 'react-redux'
import { useForm } from 'react-hook-form';

import { IonItem, IonLabel, IonInput, IonTextarea, IonButton } from '@ionic/react';

import { addDayNoema, updateDayNoema } from '../daily/daily.slice';
import { Noema } from './noema.types';

interface NoemaFormProps {
  entry?: Noema,
  onComplete: () => void
}

export default function NoemaForm({ entry, onComplete }: NoemaFormProps): JSX.Element {
  const dispatch = useDispatch();

  const { register, setValue, handleSubmit, errors } = useForm<Noema>({ defaultValues: entry });

  const onSubmit = handleSubmit((formData) => {
    // TODO: copy over the system attributes 
    // (system attributes are not exposed to the UI, since they are not editable field)
    formData.uid = entry?.uid;
    formData.highlighted = entry?.highlighted;
    formData.eventDate = entry ? entry.eventDate : (new Date()).toJSON();
    formData.kind = 'noema';

    if (entry) {
      dispatch(updateDayNoema(formData));
    } else {
      dispatch(addDayNoema(formData));
    }
    onComplete();
  });

  return (
    <form onSubmit={onSubmit}>
      Noema Form
      <IonItem>
        <IonLabel position="stacked" color="primary">Kind</IonLabel>
        <IonInput name="kind" type="text" ref={register}
          placeholder="kind"
          autocapitalize="off" required>
        </IonInput>
        <IonLabel position="stacked" color="primary">Title</IonLabel>
        <IonInput name="title" type="text" ref={register}
          placeholder="title"
          autocapitalize="off" required>
        </IonInput>
        <IonLabel position="stacked" color="primary">Content</IonLabel>
        <IonTextarea name="content" ref={register}
          placeholder="content"
          autocapitalize="off" rows={5} required>
        </IonTextarea>
        <IonLabel position="stacked" color="primary">Tags</IonLabel>
        <IonButton type="submit">Submit</IonButton>
      </IonItem>
    </form>
  )
} 
