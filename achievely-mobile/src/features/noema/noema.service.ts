import { Noema, Task } from './noema.types';

// TODO remove test when backend is implemented
const TEST_DATA_URL = '/assets/data/test-data.json';

export const getNoemas = async () => {

  const data = await fetch(TEST_DATA_URL);

  // TODO: add condition for error
  const responseData = await data.json();

  let result: Task[] = [];
  for (const entry of responseData.noemas) {
    if (entry.kind === 'noema') {
      result.push(entry as Noema);
    } else if (entry.kind === 'task') {
      result.push(entry as Task);
    }
  }

  return result;
}