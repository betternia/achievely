import React from 'react';
import { Noema } from './noema.types';
import { DateTime } from 'luxon';

import { IonItem, IonChip, IonLabel, IonIcon, IonText } from '@ionic/react';
import { happyOutline } from 'ionicons/icons';

interface NoemaDetailsProps {
  entry: Noema
}

export default function NoemaDetails({ entry }: NoemaDetailsProps): JSX.Element {

  return (
    <>
      <IonItem lines="none">
        <IonLabel>
          <div style={{ display: "flex" }}>
            <div><IonIcon icon={happyOutline} title="Edit"></IonIcon></div>
            <IonText color="primary">{DateTime.fromISO(entry.eventDate!).toLocaleString(DateTime.DATETIME_MED_WITH_SECONDS)}</IonText>
            <h1>{entry.title}</h1>
          </div>
        </IonLabel>
      </IonItem>
      <IonItem lines="none" className="ax-entry-content">
        {entry.content}
      </IonItem >
      {
        entry.tags && entry.tags.map(tag => (
          <IonChip color="dark" key={tag}>
            <IonLabel>{tag}</IonLabel>
          </IonChip>
        ))
      }

    </>
  )
} 
