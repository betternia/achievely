
export interface Attachment {
  type: string; // media type: video, audio, photo
  data: string; // url location, or the data itself
}

export enum NoemaStatus {
  ACTIVE = 1,
  PUBLISHED = 2,
  DELETED = 0,
}

/**
 * Noema is a representation of a thought.
 * Noema in this context can be a journal entry, an idea, a note, 
 * a retrospect or a task 
 */
export interface Noema
{
  _status?: number; // NoemaStatus;
  // System attributes:
  uid?: string;
  userUid?: string;
  createdDate?: string; // When the event was created
  modifiedDate?: string; // When the event was last modified
  
  // Modifiable attributes:
  kind: string;  // journal, idea, note, retro, task, ritual (set of tasks)
  eventDate: string;   // When the actual event happened, usually same as createdDate
  title: string;   // title 
  content: string; // body
  highlighted?: boolean; // body

  mood?: string | undefined;    // good, difficult, 
  location?: string | undefined;
  category?: string | undefined; // health, intellectual, emotional, spiritual, professional 
  activity?: string | undefined; // exercise, yoga, learning, friends, entertainment, etc.
  tags?: string[] | undefined;
  attachments?: Attachment[] | undefined;
  comments?: Noema[] | undefined;
}

/**
 * Task is a representation of a work.
 * It can be estimated, and completed. It can also have subTasks
 */
export interface Task extends Noema
{
  priority?: number,
  levelOfEffort?: number, // 0: no effort, 1: small 2: medium 3: large
  // targetDateTime?: string,  Use eventDate
  completedDate?: string,
  blockedSinceDate?: string,
  canceledDate?: string,

  subTasks?: Task[]
}
