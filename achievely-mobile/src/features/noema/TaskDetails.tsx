import React from 'react';
import { Task } from './noema.types';
import { DateTime } from 'luxon';

import { IonItem, IonNote, IonChip, IonLabel, IonIcon} from '@ionic/react';
import { happyOutline } from 'ionicons/icons';

interface TaskDetailsProps {
  entry: Task
}

export default function TaskDetails({entry}: TaskDetailsProps): JSX.Element {

  return (
    <>
      <IonItem >
        <IonLabel>
          <div>{DateTime.fromISO(entry.eventDate!).toLocaleString(DateTime.DATETIME_MED_WITH_SECONDS)}</div>
          <div>{entry.title}</div>
        </IonLabel>
        <IonNote slot="end">
          <IonIcon icon={happyOutline} title="Edit"></IonIcon>
        </IonNote>
      </IonItem>
      <div>
        { entry.content }
      </div>
      <IonChip color="dark">
        <IonLabel>Retro</IonLabel>
      </IonChip>
    </>
  )
} 