import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { AppThunk, AppDispatch } from '../../store'
import { Noema, NoemaStatus, Task } from './noema.types'

interface NoemaState {
  byIds: {[key:string]: Noema | Task}
  loading: boolean
}

const initialState: NoemaState = { 
  byIds: {},
  loading: false,
}

const noemasSlice = createSlice({
  name: 'noemas',
  initialState,
  reducers: {
    setNoema(state, action: PayloadAction<Noema>) {
      if (!action.payload.uid) {
        throw new Error('Item uid not provided');
      }

      // Notice the state.push(), Immer look after immutability
      state.byIds[action.payload.uid!] = action.payload;
    },
    deleteNoema(state, action: PayloadAction<Noema>) {
      if (!action.payload.uid) {
        throw new Error('Item uid not provided');
      }
      const deletedEl: Noema = {
        ...state.byIds[action.payload.uid!],
        _status: NoemaStatus.DELETED, 
        modifiedDate: action.payload.modifiedDate
      };
      
      state.byIds[action.payload.uid!] = deletedEl;
    },
    toggleHighlight(state, action: PayloadAction<string>) {
      state.byIds[action.payload].highlighted = !state.byIds[action.payload].highlighted;
    }
  }
});

/**
 * Primes a new noema by adding createDate
 * @param noema 
 * @param kind 
 */
function prepNewNoema(noema: Noema, kind: string): Noema{
  if (!noema.uid) { 
    // A simple unique ID generator
    noema.uid = Math.random().toString(36).substr(2, 9);
  }
  noema.kind = kind;
  noema.createdDate = (new Date()).toJSON();
  noema.modifiedDate = (new Date()).toJSON();
  return noema;
}

export const addNoema = (noema: Noema) : AppThunk => async(dispatch: AppDispatch) => {
  prepNewNoema(noema, 'noema');
  
  // TODO: make API request to add noema

  dispatch (noemasSlice.actions.setNoema(noema));
}

export const updateNoema = (noema: Noema) : AppThunk => async(dispatch: AppDispatch) => {
  noema.modifiedDate = (new Date()).toJSON();
  
  // TODO: make API request to add noema

  dispatch (noemasSlice.actions.setNoema(noema));
  // return originalNoema; 
}

export const deleteNoema = (noema: Noema) : AppThunk => async(dispatch: AppDispatch) => {  
  // TODO: make API request to add noema

  dispatch (noemasSlice.actions.deleteNoema(noema));
}

export const addTask = (task: Task) : AppThunk => async(dispatch: AppDispatch) => {
  prepNewNoema(task, 'task');

  // TODO: make API request to add noema
  
  dispatch (noemasSlice.actions.setNoema(task));
}

export const updateTask = (task: Task) : AppThunk => async(dispatch: AppDispatch) => {
  task.modifiedDate = (new Date()).toJSON();
  
  // TODO: make API request to add noema

  dispatch (noemasSlice.actions.setNoema(task));
  // return originalNoema; 
}

export const { toggleHighlight } = noemasSlice.actions;

export default noemasSlice.reducer;
