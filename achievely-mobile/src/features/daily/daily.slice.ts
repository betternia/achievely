import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { AppThunk, AppDispatch } from '../../store'

import { insertOrdered, binarySearch } from '../../utils/array_utils'

import { Noema, Task } from '../noema/noema.types'
import { addNoema, updateNoema, addTask, updateTask } from '../noema/noema.slice'

import { Daily } from './daily.types'



const initialState: Daily[] = [];

const dailyDateDesc = (lhs: Daily, rhs: Daily) => {
  return rhs.date.localeCompare(lhs.date);
}

const noemaEventDateDesc = (lhs: Noema, rhs: Noema) => {
  return rhs.eventDate.localeCompare(lhs.eventDate);
}

const dailySlice = createSlice({
  name: 'dailies',
  initialState,
  reducers: {
    addDaily(state, action: PayloadAction<Daily>) {
      // Notice the state.push(), Immer look after immutability
      state.push(action.payload);
    },
    addDayNoema(state, action: PayloadAction<Noema>) {
      const newDaily: Daily = {
        date: action.payload.eventDate.substring(0, 10),
      }
      const searchRes = binarySearch(state, newDaily, dailyDateDesc);
      if (searchRes !== null) {
        // A matching Daily was found, add it to it
        insertOrdered(searchRes[1].noemas!, action.payload, noemaEventDateDesc);
      } else {
        // Create a new Daily and add it
        newDaily.tasks = [];
        newDaily.noemas = [action.payload];
        const isLarger = (daily: Daily) => dailyDateDesc(daily, newDaily);
        let idx = state.findIndex(isLarger) - 1;
        idx = (idx >= -1) ? idx : state.length;
        state.splice(idx + 1, 0, newDaily);
      }

      return state;
    },
    updateDayNoema(state, action: PayloadAction<Noema>) {
      // If the eventDate has changed, move it, 
      // else, nothing to change
    },
    addDayTask(state, action: PayloadAction<Task>) {
      const newDaily: Daily = {
        date: action.payload.eventDate!.substring(0, 10),
      }
      const searchRes = binarySearch(state, newDaily, dailyDateDesc);
      if (searchRes !== null) {
        // A matching Daily was found, add it to it
        insertOrdered(searchRes[1].tasks!, action.payload, noemaEventDateDesc);
      } else {
        // Create a new Daily and add it
        newDaily.noemas = [];
        newDaily.tasks = [action.payload];
        const isLarger = (daily: Daily) => dailyDateDesc(daily, newDaily);
        let idx = state.findIndex(isLarger) - 1;
        idx = (idx >= -1) ? idx : state.length;
        state.splice(idx + 1, 0, newDaily);
      }

      return state;
    },
    updateDayTask(state, action: PayloadAction<Noema>) {
      // If the eventDate has changed, move it, 
      // else, nothing to change
    },
  }
});

export const addDayNoema = (noema: Noema): AppThunk => async (dispatch: AppDispatch) => {
  const val = await dispatch(addNoema(noema));
  // alert(`val: ${JSON.stringify(val, null, 2)}`);
  dispatch(dailySlice.actions.addDayNoema(noema));
}

export const updateDayNoema = (noema: Noema): AppThunk => async (dispatch: AppDispatch) => {
  const origEntry = await dispatch(updateNoema(noema));
  // alert(`val: ${JSON.stringify(origEntry, null, 2)}`);

  // TODO: send both original and new so that the logic can move from Daily record accordingly
  dispatch(dailySlice.actions.updateDayNoema(noema));
}

export const addDayTask = (task: Task): AppThunk => async (dispatch: AppDispatch) => {
  const val = await dispatch(addTask(task));
  dispatch(dailySlice.actions.addDayTask(task));
}

export const updateDayTask = (task: Task): AppThunk => async (dispatch: AppDispatch) => {
  const origEntry = await dispatch(updateTask(task));
  // alert(`val: ${JSON.stringify(origEntry, null, 2)}`);

  // TODO: send both original and new so that the logic can move from Daily record accordingly
  dispatch(dailySlice.actions.updateDayTask(task));
}

export default dailySlice.reducer;
