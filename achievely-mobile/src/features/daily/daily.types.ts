import { Noema, Task } from '../noema/noema.types'

export interface Daily {
  date: string;
  tasks?: Task[];
  noemas?: Noema[];
}
