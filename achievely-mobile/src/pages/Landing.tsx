import React from 'react';
import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonButtons, IonButton, IonMenuButton, IonCard, IonCardHeader, IonCardSubtitle, IonCardTitle, IonCardContent } from '@ionic/react';

// import LoginButtons from '../components/LoginButtons';
import './Landing.scss';

const Landing: React.FC = () => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
          <IonTitle>Achievely</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent fullscreen>

        <div className="landing-background">
          {/* <img src="/assets/pexels-johannes-plenio-1632790.jpg" alt="Hero" /> */}
          <IonButtons>
            {/* <IonButton color="light" fill="outline">Login</IonButton>
            <IonButton color="light" fill="outline">SignUp</IonButton> */}
            {/* <LoginButtons /> */}
          </IonButtons>
          {/* <h1>Achievely</h1> */}
        </div>

        <IonCard>
          <IonCardHeader>
            <IonCardSubtitle>Achieve your goals, introspect your life</IonCardSubtitle>
            <IonCardTitle>And Plant Trees!</IonCardTitle>
          </IonCardHeader>

          <IonCardContent>
            We are soo glad to see you.
            Are you ready to save the planet!!
            YEAH!
          </IonCardContent>
        </IonCard>

      </IonContent>
    </IonPage>
  );
};

export default Landing;
