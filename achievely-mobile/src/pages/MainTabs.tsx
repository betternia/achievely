import React from 'react';
import { IonTabs, IonRouterOutlet, IonTabBar, IonTabButton, IonIcon, IonLabel } from '@ionic/react';
import { Route, Redirect } from 'react-router';

import { calendarOutline, barChartOutline, receiptOutline, rocketOutline } from 'ionicons/icons';

import DailyTab from './Daily';
import PlaceHolder from '../components/PlaceHolder';
import EntryForm from './EntryForm';
import JournalTab from './Journal';

interface MainTabsProps { }

const MainTabs: React.FC<MainTabsProps> = () => {

  return (
    <IonTabs>
      <IonRouterOutlet>
        <Route path="/tabs/daily" component={DailyTab} exact={true} />
        <Route path="/tabs/new/:type" component={EntryForm} />
        <Route path="/tabs/edit/:uid" component={EntryForm} />
        <Route path="/tabs/goals" render={() => <PlaceHolder name="Goals" />} />
        <Route path="/tabs/stats" render={() => <PlaceHolder name="Stat" />} />
        <Route path="/tabs/cal" render={() => <PlaceHolder name="Calendar" />} />
        <Route path="/tabs" render={() => <Redirect to="/tabs/daily" />} exact={true} />
        <Route path="/tabs/journal" component={JournalTab} exact={true} />
      </IonRouterOutlet>
      <IonTabBar slot="bottom">
        <IonTabButton tab="journal" href="/tabs/daily">
          <IonIcon icon={receiptOutline} />
          <IonLabel>Journal</IonLabel>
        </IonTabButton>
        <IonTabButton tab="goals" href="/tabs/goals">
          <IonIcon icon={rocketOutline} />
          <IonLabel>Goals</IonLabel>
        </IonTabButton>
        <IonTabButton tab="stat" href="/tabs/stats">
          <IonIcon icon={barChartOutline} />
          <IonLabel>Stat</IonLabel>
        </IonTabButton>
        <IonTabButton tab="cal" href="/tabs/cal">
          <IonIcon icon={calendarOutline} />
          <IonLabel>Cal</IonLabel>
        </IonTabButton>
      </IonTabBar>
    </IonTabs>
  );
};

export default MainTabs;
