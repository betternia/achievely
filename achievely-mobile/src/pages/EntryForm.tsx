import React, { useEffect } from 'react';

import { useParams, useHistory } from "react-router-dom";

import { IonContent, IonHeader, IonBackButton, IonToggle, IonPage, IonTitle, IonToolbar, IonButtons, IonMenuButton, IonLabel, IonSearchbar } from '@ionic/react';

import { useSelector } from 'react-redux';
import { RootState } from '../store/rootReducer';

import NoemaForm from '../features/noema/NoemaForm';
import TaskForm from '../features/noema/TaskForm'
// import { leafOutline } from 'ionicons/icons';
import './Daily.css';

interface ParamType {
  type: string,
  uid: string
}

const EntryForm: React.FC = () => {

  const history = useHistory();
  const routeParams = useParams<ParamType>();

  const noemas = useSelector(
    (state: RootState) => state.noemas
  );

  const entry = routeParams.uid ? noemas.byIds[routeParams.uid] : undefined;
  // console.log('route:' + routeParams.uid, 'entry:' + JSON.stringify(entry));

  const [isTask, setIsTask] = React.useState(routeParams.type === 'task');

  useEffect(() => {
    setIsTask(routeParams.type === 'task');
  }, [setIsTask])

  return (
    <IonPage>
      <IonHeader translucent={true}>
        <IonToolbar>
          <IonButtons slot="start">
            <IonBackButton defaultHref="/tabs/daily" />
          </IonButtons>
          <IonTitle>Journal</IonTitle>
          {false && <IonSearchbar showCancelButton="always" placeholder="Search" ></IonSearchbar>}

        </IonToolbar>
      </IonHeader>

      <IonContent fullscreen className="card-background-page">
        <IonLabel>Is Task</IonLabel>
        <IonToggle checked={isTask} onClick={() => setIsTask(!isTask)} />
        {!isTask && <NoemaForm entry={entry} onComplete={() => { history.push("/tabs/daily"); }} />}
        {isTask && <TaskForm entry={entry} onComplete={() => { history.push("/tabs/daily"); }} />}
      </IonContent>
    </IonPage>
  );
};

export default EntryForm;
