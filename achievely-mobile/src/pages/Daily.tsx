import React from 'react';
import { useHistory } from "react-router-dom";

import { IonContent, IonHeader,  IonIcon, IonPage, IonTitle, IonToolbar, IonButtons, IonMenuButton, IonSearchbar, IonFab, IonFabList, IonFabButton } from '@ionic/react';

import ContentHeader from '../components/ContentHeader';
import DailyList from '../features/daily/DailyList'
import { addOutline, receiptOutline, rocketOutline } from 'ionicons/icons';
import './Daily.css';

/**
 * Action Button
 */
const AddFab: React.FC = () => {

  const history = useHistory();

  const openForm = (type: string) => {
    history.push(`/tabs/new/${type}`);
  };

  return(
    <>
      <IonFab slot="fixed" vertical="bottom" horizontal="center">
        <IonFabButton>
          <IonIcon icon={addOutline} />
        </IonFabButton>
        <IonFabList side="top">
          <IonFabButton color="primary" onClick={() => openForm('noema')}>
            <IonIcon icon={receiptOutline} />
          </IonFabButton>
          <IonFabButton color="secondary" onClick={() => openForm('task')}>
            <IonIcon icon={rocketOutline} />
          </IonFabButton>
        </IonFabList>
      </IonFab>
    </>
  )
};

const Daily: React.FC = () => {

  // console.log( 'session:' + JSON.stringify(session, null, 2));

  return (
    <IonPage>
      <IonHeader translucent={true}>
        <IonToolbar>
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
          <IonTitle>Journal</IonTitle>
          { false && <IonSearchbar showCancelButton="always" placeholder="Search" ></IonSearchbar>}
          
        </IonToolbar>
      </IonHeader>

      <IonContent fullscreen className="card-background-page">
        <ContentHeader />
        <DailyList />
        
      </IonContent>
      <AddFab />
    </IonPage>
  );
};

export default Daily;
