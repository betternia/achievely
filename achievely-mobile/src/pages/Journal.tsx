import React from 'react';

import { useHistory } from "react-router-dom";

import { 
  IonButtons,
  IonContent,
  IonHeader,  
  IonPage, 
  IonTitle,
  IonToolbar,
  IonMenuButton
} from '@ionic/react';

import NoemaForm from '../features/noema/NoemaForm';
import NoemaList from '../features/noema/NoemaList'
import './Journal.css';

const Tab1: React.FC = () => {

  const history = useHistory();

  return (
    <IonPage>
      <IonHeader translucent={true}>
        <IonToolbar>
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
          <IonTitle>Journal</IonTitle>
          
        </IonToolbar>
      </IonHeader>

      <IonContent fullscreen className="card-background-page">

        <NoemaForm onComplete={ () => { history.push("/journal"); } } />
        <hr />
        <NoemaList />
      </IonContent>
    </IonPage>
  );
};

export default Tab1;
