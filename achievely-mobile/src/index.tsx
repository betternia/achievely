import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import * as serviceWorker from './serviceWorker';

import store from './store'

// TEST: Load test data on page load
if (process.env.NODE_ENV === 'development') {
  const dailySlice = require('./features/daily/daily.slice')
  import('./features/noema/noema.service')
  .then(noemaService => {
    return noemaService.getNoemas();
  })
  .then(noemas => {
    for (const noema of noemas) {
      if (noema.kind === 'noema') {
        store.dispatch(dailySlice.addDayNoema(noema));
      } else {
        store.dispatch(dailySlice.addDayTask(noema));
      } 
    }
  })
  .catch(error => {
    console.error('Error while loading test data: %j', error);
  });
}

function render() {
  // This is just following the hot reload sample
  const App = require('./App').default 
  ReactDOM.render(
    <Provider store = {store}>
      <App />
    </Provider>, 
    document.getElementById('root'));
}

render();

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
